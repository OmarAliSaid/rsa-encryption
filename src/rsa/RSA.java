/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rsa;
import java.util.ArrayList;
import java.util.Random;
import java.util.Vector;
/**
 *
 * @author Ahmed Dongl
 */
public class RSA {

   public static int gcd_extended_ecul(int r1,int r0){
        Vector<Integer>r=new Vector<>();
        Vector<Integer>t=new Vector<>();
        Vector<Integer>q=new Vector<>();
        Vector<Integer>s=new Vector<>();
                
        int i=1;
        
        s.add(1); s.add(0); t.add(0); t.add(1); q.add(1);q.add(1);r.add(r0);r.add(r1);
        do{
            i++;
            r.add(r.get(i-2)%r.get(i-1));
            q.add(r.get(i-2)/r.get(i-1));
            t.add(t.get(i-2)-q.get(i)*t.get(i-1));
            s.add(s.get(i-2)-q.get(i)*s.get(i-1));
        }while(r.get(i)!=0);
        int res=r.get(i-1);
        while(res<0)
        {res+=r0;}
        return res;
    }
    
   static  int square_multiply(int x1,int p1,int n1){
        long x=x1,n=n1;
        long z=x;
        String a=Integer.toBinaryString(p1);
        for(int i=1;i<a.length();i++){
            z*=z;
            if(a.charAt(i)=='1'){
                z%=n;
                z*=x;
            }
            z%=n;
        }
        return (int)z;
    }
    
   static int extended_euc(int r1,int r0){
        Vector<Integer>r=new Vector<>();
        Vector<Integer>t=new Vector<>();
        Vector<Integer>q=new Vector<>();
        Vector<Integer>s=new Vector<>();
                
        int i=1;
        
        s.add(1); s.add(0); t.add(0); t.add(1); q.add(1);q.add(1);r.add(r0);r.add(r1);
        
        do{
            i++;
            r.add(r.get(i-2)%r.get(i-1));
            q.add(r.get(i-2)/r.get(i-1));
            t.add(t.get(i-2)-q.get(i)*t.get(i-1));
            s.add(s.get(i-2)-q.get(i)*s.get(i-1));
        }while(r.get(i)!=0);
        
        int res=t.get(i-1);
        
        while(res<0)
        {res+=r0;}
        
        return res;
    }
    
   private static  void Play(String text) {                                         

        Random r=new Random();
        for(int i=0;i<text.length();i++){
            int p=0,q=0,e,d=2,n,a,phi_n,s=100;
            int X=text.charAt(i);
            boolean prime=false;
            System.out.println("Char # "+(i+1));
            System.out.println(X+" ");
            
            
            while(!prime){
                p=r.nextInt(32766-(X+1))+X+1;
                prime=true;
                for(int j=0;j<s;j++){
                    a=r.nextInt(p-4)+2;
                    if(square_multiply(a, p-1, p)!=1){
                        prime=false;
                        break;
                    }
                }
            }
            prime=false;
            while(!prime){
                q=r.nextInt(32766-(X+1))+X+1;
                if(q==p)
                {
                    continue;
                }
                prime=true;
                
                for(int j=0;j<s;j++){
                    a=r.nextInt(q-4)+2;
                    if(square_multiply(a, q-1, q)!=1){
                        prime=false;
                        break;
                    }
                }
            }

            
            n=p*q;
            phi_n=(p-1)*(q-1);
    
            for(e=2;e<phi_n;e++){
                if(gcd_extended_ecul(e,phi_n)==1)
                    break;
            }
            
            d=extended_euc(e,phi_n);

            int enc=square_multiply(X, e, n);

            System.out.println("p: "+p+" q: "+q+" e: "+e+" d: "+d+" n: "+n+"\n");
            System.out.println("encrypt");
            System.out.println(enc+" ");
            X=enc;
            int xp=X%p,
                xq=X%q,
                dp=d%(p-1),
                dq=d%(q-1);
            long yp=square_multiply(xp, dp, p);
            long yq=square_multiply(xq, dq, q);
            
            long cp=extended_euc(q, p);
            long cq=extended_euc(p, q);
            
            int y=(int)((((q*cp)%n)*yp+((p*cq)%n)*yq)%((long)n));
            System.out.println("Decrypt is ");
            System.out.println(y+" ");
        }
        
    }                                        
    
   public static void main(String args[]) {
        
        Play("abc");
    }
    
    
}
